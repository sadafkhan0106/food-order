package com.food.order.controller;

import com.food.order.dto.UserDto;
import com.food.order.service.IUserService;
import com.food.order.storage.User;
import com.food.order.storage.VerificationToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.Date;
import java.util.Optional;

@Controller
@RequestMapping("/")
public class UserController {
    @Autowired
    IUserService service;

    @GetMapping("/")
    public String home(){
        return "index";
    }

    @GetMapping("/index")
    public String index(){
        return "index";
    }
    @GetMapping("/login")
    public String login(@RequestParam Optional<Boolean> error) {
        return "login";
    }

    @GetMapping("/registration")
    public String getRegistrationForm(Model model){
        UserDto userDto = new UserDto();
        model.addAttribute("user", userDto);
        return "reg";
    }

    @PostMapping("/registration")
    public ModelAndView registerUserAccount(@ModelAttribute("user") @Valid UserDto userDto) throws Exception {
        User registered = service.registerUser(userDto);
        return new ModelAndView("login","user",userDto);
    }

    @GetMapping("/registrationConfirm")
    public String confirmRegistration(Model model,@RequestParam("token") String token){
        VerificationToken verificationToken = service.getVerificationToken(token);
        if(verificationToken == null){
            model.addAttribute("message", "invalid verification token");
            return "badUser";
        }
        if(verificationToken.getExpiryTime().getTime()-new Date().getTime()<=0){
            model.addAttribute("message", "invalid verification token");
            return "badUser";
        }
        User user = verificationToken.getUser();
        user.setEnabled(true);
        service.saveRegisteredUser(user);
        return "login";
    }

    @GetMapping("/about")
    public String about(){
        return "about";
    }

    @GetMapping("/blog")
    public String stories(){
        return "blog";
    }

    @GetMapping("/contact")
    public String contact(){
        return "contact";
    }

    @GetMapping("/menu")
    public String menu(){
        return "menu";
    }

    @GetMapping("/reservation")
    public String reservation(){
        return "reservation";
    }

}
